public class ContactSearch 
{
    public static List<Contact> searchForContacts(String lastname,String mailingpostalcode)
    {
        List<Contact> cts1=[select Id,Name,LastName,MailingPostalCode 
                            from Contact
                            where LastName=:lastname and MailingPostalCode=:mailingpostalcode];
        return cts1;
        
    }

}