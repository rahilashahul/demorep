public class RandomContactFactory 
{
    public static List<Contact> generateRandomContacts(Integer numofcontacts,String lastnamecon)
    {
        List<Contact> con=new List<Contact>();
        for(Integer i=0;i<numofcontacts;i++)
        {
            String unifirstname='Test'+i;
            Contact c1=new Contact(FirstName=unifirstname,LastName=lastnamecon);
            con.add(c1);
        }
        return con;
    }

}