<aura:application extends="force:slds">
    <!-- This is the real app --> 
    <c:expenses />
    <c:expensesList expenses="{!v.expenses}"/>
</aura:application>