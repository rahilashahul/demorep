public class LTNG_CommunityLink {
    @AuraEnabled(cacheable=true)
    public static List<responseWrapper> getCommunityBaseUrl(){
        List<Id> netid = new List<Id>();
        List<responseWrapper> cname = new List<responseWrapper>();
        for(NetworkMember nm : [Select NetworkId from NetworkMember where MemberId =: userinfo.getuserid()])
        {
            netid.add(nm.NetworkId);
        }
        for(Network n : [Select Id,Name,Status,UrlPathPrefix from Network where Id in: netid AND Status=:'Live'])
        {
            ConnectApi.Community  myCommunity = ConnectApi.Communities.getCommunity(n.id);
            System.debug('MyDebug : ' + myCommunity.siteUrl);
            
            
            responseWrapper res = new responseWrapper();
            res.communityName = n.Name;
            res.communityURL =myCommunity.siteUrl;
            cname.add(res);
            
        }
        System.debug(cname);
        //return ConnectApi.Communities.getCommunities().communities;
        return cname;
    }
    
    public class responseWrapper{
        @AuraEnabled
        public String communityName;
        @AuraEnabled
        public String communityURL;
    }
}