public class CreateNewUser{
 public User user;
public CreateNewUser(ApexPages.StandardController controller)
{ 
  this.user= (User)controller.getrecord();
}
public List<selectOption> getLicense()
{
    List<selectOption> options = new List<selectOption>();
    options.add(new selectOption('', '- None -')); 
     for (UserLicense users :[SELECT Id,Name FROM UserLicense])  
      {    
            options.add(new selectOption(users.Id, users.Name)); 
      }
    return options; 
}

public List<selectOption> getProfile() {
  List<selectOption> options1 = new List<selectOption>(); 
  options1.add(new selectOption('', '- None -'));
  for (Profile users1 :[SELECT Id,Name FROM Profile])  { 
          options1.add(new selectOption(users1.Id, users1.Name)); 
   } 
    return options1;
} 
}