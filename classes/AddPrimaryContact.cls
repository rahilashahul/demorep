public class AddPrimaryContact implements Queueable{
    private Contact c;
    private String s;
    public AddPrimaryContact(Contact c1,String State)
    {
        this.c = c1;
        this.s = State;
    }
    public void execute(QueueableContext Context)
    {
        List<Account> acclist = [Select Id from Account where BillingState =: s limit 200];
        List<Contact> c2 = new List<Contact>();
        for(Account acct:acclist)
        {
            Contact c3 = c.clone(false,false,false,false);
            c3.AccountId = acct.Id;
            c2.add(c3);
        }
        insert c2;
    }
}