@isTest
public class AccountManagerTest {
    public static testmethod void testmethod1()
    {
        Account a = new Account(Name = 'Test Account');
        insert a;
        Contact c = new Contact(LastName = 'Test');
        c.AccountId = a.Id;
        insert c;
        RestRequest request = new RestRequest();
        request.requestUri ='https://rahilashahul-dev-ed.my.salesforce.com//services/apexrest/Accounts/'+ a.Id +'/contacts';
        request.httpMethod = 'GET';
        RestContext.request = request;
        Account thisAccount = AccountManager.getAccount();
        System.assert(thisAccount!= null);
        System.assertEquals('Test Account', thisAccount.Name);

    }

}