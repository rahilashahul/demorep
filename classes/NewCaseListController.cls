public class NewCaseListController {
    public List<Case> getNewCases()
    {
        List<Case> cases=[Select Id,CaseNumber from Case where Status = 'New'];
        return cases;
    }

}