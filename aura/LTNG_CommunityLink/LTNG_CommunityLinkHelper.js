({
    validateRecordId : function(component, event, helper) { 
        console.log('validateRecordId caleld.. '); 
        var recId = component.get('v.recordId');
        console.log('recordId : '+recId); 
        var isValidRecord = (recId && (recId.length==15 || recId.length==18));
        component.set("v.isValidRecord", isValidRecord);
        var fullURL = '';
        if(isValidRecord){
            var commURL = component.get('v.commURL');
            fullURL = commURL.replace("login", "s/detail/")+recId;
        } 
        component.set("v.fullURL", fullURL);  
    }, 
    
    copyText :function(component, event, helper) {
        var fullURL = component.get('v.fullURL'); 
        var hiddenInput = document.createElement("input"); 
        hiddenInput.setAttribute("value", component.get('v.selectedValue')+"/"+component.get("v.recordId")); 
        document.body.appendChild(hiddenInput); 
        hiddenInput.select(); 
        document.execCommand("copy"); 
        document.body.removeChild(hiddenInput);  
        var copyBtn = event.getSource(); 
        var origLabel = copyBtn.get('v.label');
        copyBtn.set('v.label','Copied!');
        window.setTimeout(
            $A.getCallback(function() {
                copyBtn.set('v.label', origLabel);
            }), 2500
        ) 
    },
})