trigger BatchApexErrorTrigger on BatchApexErrorEvent (after insert) {
 BatchLeadConvertErrors__c [] insertErrorList = New BatchLeadConvertErrors__c[]{};
     for(BatchApexErrorEvent err : Trigger.New){
         BatchLeadConvertErrors__c tmplog = New BatchLeadConvertErrors__c();
         tmplog.AsyncApexJobId__c = err.AsyncApexJobId;
         tmplog.Records__c = err.JobScope;
         tmplog.StackTrace__c = err.StackTrace;
         insertErrorList.add(tmplog);
     }
 insert insertErrorList;
}