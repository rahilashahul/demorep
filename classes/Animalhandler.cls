@RestResource (UrlMapping='/animalrestapihandler/')
global class Animalhandler {
@HttpGet
    global static Animal__c doget()
    {
        Animal__c a = new Animal__c();
        Map<String,String> a1 =RestContext.request.Params;
        String b = a1.get('Id');
        a = [Select Id,Name from Animal__c where Id =: b];
        return a;
    }
    @HttpDelete
    global static String dodelete()
    {
        Animal__c b1 = new Animal__c();
        Map<String,String> a2 =RestContext.request.Params;
        String b2 = a2.get('Id');
        b1 = [Select Id,Name from Animal__c where Id =: b2];
        Delete b1;
        return 'Animal Deleted!';
    }
}