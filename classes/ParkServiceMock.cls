@isTest
global class ParkServiceMock implements WebServiceMock{
    global void doinvoke(Object stub,Object request,Map<String, Object> response,String endpoint,String soapAction,String requestName,String responseNS,String responseName,String responseType)
    {
        ParkService.byCountryResponse res = new ParkService.byCountryResponse();
        List<String> myStrings = new List<String> {'Park1','Park2','Park3'};
        res.return_x = myStrings;
        response.put('response_x', res);
    }

}