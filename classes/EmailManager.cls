public class EmailManager {
    public static void sendMail(String address,String subject,String body)
    {
        Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
        String[] toaddr=new String[] {address};
        mail.setToAddresses(toaddr);
        mail.setSubject(subject);
        mail.setPlainTextBody(body);
        
        System.debug('Mail record is '+mail);
        System.debug('Mail record is '+mail.getSubject());
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            
    }

}