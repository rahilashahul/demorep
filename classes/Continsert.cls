public class Continsert 
{
 public static void insert_cont()
 {
     //To insert multiple contacts
     List<Contact> cont=new List<Contact>{
         new Contact(FirstName='Sharan',LastName='Mary',Department='CSE'),
         new Contact(FirstName='Sherine',LastName='Immaculate',Department='ECE'),
         new Contact(FirstName='Reena',LastName='James',Department='CSE')   
     };
     
     //DML Operation on Contact
     insert cont;
     
     //To Update Contacts
     List<Contact> conupd=new List<Contact>();
     for(Contact Con:cont)
     {
         if(Con.Department=='CSE')
         {
             Con.Department='EEE';
         }
         conupd.add(Con);
     }
     update conupd;
 }

}