trigger AccountTrigger on Account (before insert) {
    if(Trigger.isbefore && Trigger.isinsert){
        AccountTriggerHandler.CreateAccounts(Trigger.New);
    }
}