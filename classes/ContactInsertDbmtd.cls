public class ContactInsertDbmtd 
{
    public static void contup1()
    {
       List<Contact> ct1=new List<Contact>
       {
           new Contact(FirstName='Priya',LastName='Tharshini',Email='priya23@gmail.com'),
           new Contact(FirstName='Ragavee',LastName='Brindha',Email='brindha43@gmail.com'), 
           new Contact(FirstName='Noori',LastName='Nisha',Email='nisha12@gmail.com') ,
           new Contact()
       };
           Database.SaveResult[] sr1=Database.insert(ct1,false);
           for(Database.SaveResult sr:sr1)
           {
               if(sr.isSuccess())
                {
                     System.debug('Successfully inserted contact. Contact ID:'+sr.getId());
                }
               else
                {
                     for(Database.Error err:sr.geterrors())
                     {
                             System.debug('The following error has occurred.');
                             System.debug(err.getStatusCode() + ': ' + err.getMessage());
                             System.debug('Contact fields that affected this error: ' + err.getFields());
                     }
                
                }
           }
      
    }
}