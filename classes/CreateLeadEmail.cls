global class CreateLeadEmail implements Messaging.InboundEmailHandler {
    //Messaging.InboundEmailHandler interface to handle an inbound email messages
global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email,Messaging.InboundEnvelope env){
    //handleInboundEmail method is to access the InboundEmail object to retrieve the contents, headers, and attachments of inbound email messages
    Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
    // Add the from email address into the local variable
    String em = email.fromAddress;
    // Add the email plain text into the local variable
    String body = email.plainTextBody == null? (email.htmlBody == null? '': email.htmlBody.stripHtmlTags()): email.plainTextBody;
    List<Lead> leadlist = new List<Lead>();
    try{
        leadlist.add(new lead(Email = email.fromAddress,LastName =email.fromName,OwnerId = '0056F000008WpQCQA0',CurrencyIsoCode = 'USD U.S. Dollar',Status = 'Working - Contacted'));
        insert leadlist;
    }
    catch(QueryException e){
        System.debug('Query Issue: ' + e);
    }
    result.success = true;
    return result;
}
}